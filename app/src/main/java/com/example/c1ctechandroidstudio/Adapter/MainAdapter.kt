import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.c1ctechandroidstudio.DATA.Search
import com.example.c1ctechandroidstudio.R
import com.example.c1ctechandroidstudio.databinding.LayoutRvItemBinding



class MainAdapter : RecyclerView.Adapter<MainViewHolder>() {

    var movies = mutableListOf<Search>()
    fun setMovieList(movies: List<Search>) {

        this.movies = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutRvItemBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val movie = movies[position]
        holder.binding.Title.text = movie.Title
        Glide.with(holder.itemView.context).load(movie.Poster).placeholder(R.drawable.placeholder)
            .into(holder.binding.Poster)



    }
    override fun getItemCount(): Int {
        return movies.size
    }
}


class MainViewHolder(val binding: LayoutRvItemBinding) : RecyclerView.ViewHolder(binding.root) {}