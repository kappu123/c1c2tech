package com.example.c1ctechandroidstudio.Repository

import com.example.c1ctechandroidstudio.Interface.RetrofitService


class MainRepository constructor(private  val retrofitService: RetrofitService) {


    fun getAllMovies() = retrofitService.getAllMovies()
}