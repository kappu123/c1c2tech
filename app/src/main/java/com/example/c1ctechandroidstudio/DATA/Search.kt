package com.example.c1ctechandroidstudio.DATA

data class Search(

    val Poster: String,
    val Title: String,
    val Year: String,
    val imdbID: String
)