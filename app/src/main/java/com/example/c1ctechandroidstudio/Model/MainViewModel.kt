package com.example.c1ctechandroidstudio.Model
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.c1ctechandroidstudio.DATA.MovieList
import com.example.c1ctechandroidstudio.DATA.Search
import com.example.c1ctechandroidstudio.Repository.MainRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(private val repository: MainRepository) : ViewModel() {




    val movieList = MutableLiveData<List<Search>>()
    val errorMessage = MutableLiveData<String>()



    fun getAllMovies() {
        val response = repository.getAllMovies()
        response.enqueue(object : Callback<MovieList> {

            override fun onResponse(call: Call<MovieList>, response: Response<MovieList>) {
            movieList.postValue(response.body()!!.Search)
            }

            override fun onFailure(call: Call<MovieList>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}

