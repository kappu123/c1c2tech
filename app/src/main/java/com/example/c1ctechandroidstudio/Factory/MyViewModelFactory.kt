package com.example.c1ctechandroidstudio.Factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.c1ctechandroidstudio.Model.MainViewModel
import com.example.c1ctechandroidstudio.Repository.MainRepository

class MyViewModelFactory constructor(private val repository: MainRepository) :
    ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {


        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            MainViewModel(this.repository) as T


        } else {
            throw IllegalArgumentException(" kapil is not working ")
        }
    }
}